using Microsoft.AspNetCore.Mvc;
using SignOUT.Models.UsersViewModels;
using SignOUT.Services;
using System.Collections.Generic;
using System.Linq;

namespace SignOUT.Views
{
    public class UsersController : Controller
    {

        private IUsersRepository _usersRepository;

        public UsersController(IUsersRepository usersRepository)
        {
            _usersRepository = usersRepository;
        }

        public IActionResult Index()
        {
            var usersVM = from user in _usersRepository.ListAll()
                          select new IndexViewModel()
            {
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName
            };

            return View(usersVM);
        }
    }
}