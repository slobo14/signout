﻿using Microsoft.AspNetCore.Mvc;
using SignOUT.Models.HomeViewModels;
using SignOUT.Services;

namespace SignOUT.Controllers
{
    public class HomeController : Controller
    {
        private IEmailSender _emailSender;

        public HomeController(IEmailSender emailSender)
        {
            _emailSender = emailSender;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Send Us a message";
            ViewData["ReturnUrl"] = "/home";

            return View(new ContactViewModel());
        }

        [HttpPost]
        public IActionResult Contact(ContactViewModel contact)
        {
            ViewData["Message"] = "Message sent successfully!";

            _emailSender.SendEmailAsync("slobodan.dzakic@gmail.com", contact.Subject + " from " + contact.Name, $"Message sent by: {contact.Name} ( {contact.Email} )\n\n{contact.Message}");

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
