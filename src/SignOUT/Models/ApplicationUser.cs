﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;

namespace SignOUT.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }
        
        public DateTime Birthday { get; set; }

        public string Adress { get; set; }

        public string Country { get; set; }

        public string City { get; set; }

        public string PostCode { get; set; }

        public string FacebookId { get; set; }
    }
}
