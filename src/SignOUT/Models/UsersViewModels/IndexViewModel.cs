﻿using System.ComponentModel.DataAnnotations;

namespace SignOUT.Models.UsersViewModels
{
    public class IndexViewModel
    {
        [EmailAddress]
        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}
