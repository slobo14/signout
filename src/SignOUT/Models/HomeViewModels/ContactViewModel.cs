﻿using System.ComponentModel.DataAnnotations;

namespace SignOUT.Models.HomeViewModels
{
    public class ContactViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Subject { get; set; }

        [Required]
        public string Message { get; set; }
    }
}
