﻿using System;
using SignOUT.Models;
using SignOUT.Services;
using System.Collections.Generic;

namespace SignOUT.Data
{
    public class UsersRepository : IUsersRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public UsersRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IEnumerable<ApplicationUser> ListAll()
        {
            return _dbContext.GetAllUsers();
        }
    }
}
