﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SignOUT.Data;
using SignOUT.Models;
using SignOUT.Services;

namespace SignOUT
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            if (env.IsDevelopment())
            {
                builder.AddUserSecrets();
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseNpgsql(Configuration.GetConnectionString("PostgreConnection"), b => b.MigrationsAssembly("SignOUT")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddMvc();

            services.AddTransient<IEmailSender, AuthMessageSender>();
            services.AddTransient<IUsersRepository, UsersRepository>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseIdentity();

            app.UseFacebookAuthentication(new FacebookOptions()
            {
                AppId = Configuration["Authentication:Facebook:AppId"],
                AppSecret = Configuration["Authentication:Facebook:AppSecret"]
            });


            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

                routes.MapRoute(
                    name: "home",
                    template: "home",
                    defaults: new {controller = "Home", action = "Index"});

                routes.MapRoute(
                    "contact",
                    "contact",
                    new {controller = "Home", action = "Contact"});

                routes.MapRoute(
                    "users",
                    "users",
                    new {controller = "Users", action = "Index"});

                routes.MapRoute(
                    "log-in",
                    "log-in",
                    new {controller = "Account", action = "Login"});

                routes.MapRoute(
                    "log-off",
                    "log-off",
                    new {controller = "Account", action = "LogOff"});

                routes.MapRoute(
                    "register",
                    "register",
                    new {controller = "Account", action = "Register"});

                routes.MapRoute(
                    name: "profile",
                    template: "profile",
                    defaults: new {controller = "Manage", action = "Index"});

                routes.MapRoute(
                    "change-password",
                    "change-password",
                    new {controller = "Manage", action = "ChangePassword"});

                routes.MapRoute(
                    "set-password",
                    "set-password",
                    new { controller = "Manage", action = "SetPassword" });

                routes.MapRoute(
                    "manage-logins",
                    "manage-logins",
                    new { controller = "Manage", action = "ManageLogins" });
            });
        }
    }
}
