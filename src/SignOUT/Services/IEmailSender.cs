﻿using System.Threading.Tasks;

namespace SignOUT.Services
{
    public interface IEmailSender
    {
        Task<int> SendEmailAsync(string email, string subject, string message);
    }
}
