﻿using SignOUT.Models;
using System.Collections.Generic;

namespace SignOUT.Services
{
    public interface IUsersRepository
    {
        IEnumerable<ApplicationUser> ListAll();
    }
}
