﻿using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;
using MimeKit.Text;
using System.Net;
using System.Threading.Tasks;

namespace SignOUT.Services
{
    public class AuthMessageSender : IEmailSender
    {
        public async Task<int> SendEmailAsync(string email, string subject, string message)
        {
            var emailMessage = new MimeMessage();

            var credentials = new NetworkCredential
            {
                UserName = "dzontra.vutra@gmail.com",
                Password = "smokilili1"
            };

            emailMessage.From.Add(new MailboxAddress("SignOUT", credentials.UserName));
            emailMessage.To.Add(new MailboxAddress("", email));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart(TextFormat.Html) { Text = message };
            
            using (var client = new SmtpClient())
            {
                client.LocalDomain = "smtp.gmail.com";
                await client.ConnectAsync("smtp.gmail.com", 587, SecureSocketOptions.StartTls).ConfigureAwait(false);
                await client.AuthenticateAsync(credentials);
                await client.SendAsync(emailMessage).ConfigureAwait(false);
                await client.DisconnectAsync(true).ConfigureAwait(false);
            }
            
            return 0;
        }
    }
}
